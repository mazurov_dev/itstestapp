package app.mazurov.its.itstestapp.network.model

data class Search(
        val Search: List<SearchResult>,
        val totalResults: String,
        val Response: String
)