/*
 * @author Mark Mazurov
 * @email mazurov.developer@gmail.com
 * Created 10.08.2018
 */
package app.mazurov.its.itstestapp.network

import app.mazurov.its.itstestapp.network.model.Result
import app.mazurov.its.itstestapp.network.model.Search
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


private const val API_URL = "https://www.omdbapi.com"

object Repository {


    private val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    private val client = OkHttpClient.Builder().addInterceptor { chain ->
        val newRequest = chain.request().newBuilder().build()
        chain.proceed(newRequest)
    }.addInterceptor(logging).build()

    private val retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    private val service = retrofit.create(ApiService::class.java)


    fun search(q: String, success: (search: Search) -> Unit, failure: (failure: String) -> Unit) {
        service.searchFilm(q).enqueue(object : Callback<Search> {
            override fun onFailure(call: Call<Search>?, t: Throwable?) {
                failure(t!!.message!!)
            }

            override fun onResponse(call: Call<Search>?, response: Response<Search>?) {
                if (response != null) {
                    response.body()?.let { success(it) }
                }
            }

        })
    }

    fun getFullInformation(i: String, success: (information: Result?) -> Unit, failure: (failure: String) -> Unit) {
        service.getFullInformation(i).enqueue(object : Callback<Result> {
            override fun onFailure(call: Call<Result>?, t: Throwable?) {
                failure(t!!.message!!)
            }

            override fun onResponse(call: Call<Result>?, response: Response<Result>?) {
                if (response != null) {
                    response.body()?.let { success(it) }
                } else success(null)
            }

        })
    }
}