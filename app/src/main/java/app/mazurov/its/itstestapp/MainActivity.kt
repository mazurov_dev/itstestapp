package app.mazurov.its.itstestapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.mazurov.its.itstestapp.ui.fullinformation.FullInformationFragment
import app.mazurov.its.itstestapp.ui.search.SearchFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, SearchFragment.newInstance())
                    .commit()
        }
    }

    fun openMovie(movieId: String) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, FullInformationFragment.newInstance(movieId))
                .addToBackStack(null)
                .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) supportFragmentManager.popBackStack()
        else super.onBackPressed()
    }

}
