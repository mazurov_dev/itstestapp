package app.mazurov.its.itstestapp.ui.search

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import app.mazurov.its.itstestapp.MainActivity
import app.mazurov.its.itstestapp.R
import kotlinx.android.synthetic.main.search_fragment.*

class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private lateinit var viewModel: SearchViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)

        et_search.setOnEditorActionListener { et, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if(et.text.isNotEmpty()) {
                    hideKeyboard()
                    hideViews()
                    viewModel.search(et.text.toString())
                }
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }


        val adapter = SearchAdapter(emptyList()) {
            (activity as MainActivity).openMovie(it)
        }
        rv_movies.layoutManager = LinearLayoutManager(context)
        rv_movies.adapter = adapter

        viewModel.movies.observe(this, Observer {
            if(it.Response == "False") movieNotFound()
            else {
                rv_movies.visibility = View.VISIBLE
                adapter.movies = it.Search
            }
        })

        viewModel.isLoading.observe(this, Observer {
            if(it) pb_loading_movies.visibility = View.VISIBLE
            else pb_loading_movies.visibility = View.GONE
        })

        viewModel.isLoadingFailed.observe(this, Observer {
            if(it) loadFailed()
        })

        btn_try_again.setOnClickListener {
            if(et_search.text!!.isNotEmpty()) {
                hideKeyboard()
                hideViews()
                viewModel.search(et_search.text.toString())
            }
        }
    }

    private fun movieNotFound() {
        rv_movies.visibility = View.INVISIBLE
        tv_error.text = context!!.getString(R.string.movie_not_found)
        tv_error.visibility = View.VISIBLE
    }

    private fun loadFailed() {
        rv_movies.visibility = View.INVISIBLE
        tv_error.text = context!!.getString(R.string.we_got_some_problem)
        tv_error.visibility = View.VISIBLE
        btn_try_again.visibility = View.VISIBLE
    }

    private fun hideViews() {
        rv_movies.visibility = View.INVISIBLE
        tv_error.visibility = View.GONE
        btn_try_again.visibility = View.GONE
    }

    private fun hideKeyboard() {
        val imm = this.activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = this.activity!!.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) view = View(activity)

        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
