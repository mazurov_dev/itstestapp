package app.mazurov.its.itstestapp.ui.fullinformation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import app.mazurov.its.itstestapp.MainActivity
import app.mazurov.its.itstestapp.R
import app.mazurov.its.itstestapp.network.model.Result
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.full_information_fragment.*

private const val ARG_MOVIE_ID = "argMovieId"

class FullInformationFragment : Fragment() {

    private var movieId: String = ""

    companion object {
        @JvmStatic
        fun newInstance(movieId: String) =
                FullInformationFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_MOVIE_ID, movieId)
                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movieId = it.getString(ARG_MOVIE_ID) ?: ""
        }
    }

    private lateinit var viewModel: FullInformationViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.full_information_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white)

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        viewModel = ViewModelProviders.of(this).get(FullInformationViewModel::class.java)
        viewModel.getMovie(movieId)
        viewModel.movie.observe(this, Observer {
            setInformation(it)
        })

        viewModel.isLoading.observe(this, Observer {
            if (it) pb_loading_full_information.visibility = View.VISIBLE
            else pb_loading_full_information.visibility = View.GONE
        })

        viewModel.isLoadingFailed.observe(this, Observer {
            if (it) displayErrorToast()
        })
    }


    private fun displayErrorToast() {
        Toast.makeText(context, R.string.load_full_information_error, Toast.LENGTH_SHORT).show()
    }

    private fun setInformation(movie: Result) {
        Picasso.get().load(movie.Poster).into(iv_cover)

        collapsing_toolbar.title = movie.Title

        tv_description.text = String.format(context!!.getString(R.string.description), movie.Plot)
        tv_country.text = String.format(context!!.getString(R.string.country), movie.Country)
        tv_director.text = String.format(context!!.getString(R.string.director), movie.Director)
        tv_rating.text = String.format(context!!.getString(R.string.rating), movie.imdbRating)
        tv_actors.text = String.format(context!!.getString(R.string.actors), movie.Actors)
    }

}
