/*
 * @author Mark Mazurov
 * @email mazurov.developer@gmail.com
 * Created 10.08.2018
 */

package app.mazurov.its.itstestapp.network

import app.mazurov.its.itstestapp.network.model.Result
import app.mazurov.its.itstestapp.network.model.Search
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {

    @GET("/")
    fun searchFilm(@Query("s") i: String, @Query("apikey") apikey : String = "74c226f2"): Call<Search>

    @GET("/")
    fun getFullInformation(@Query("i") i: String, @Query("plot") plot: String = "full", @Query("apikey") apikey : String = "74c226f2"): Call<Result>
}