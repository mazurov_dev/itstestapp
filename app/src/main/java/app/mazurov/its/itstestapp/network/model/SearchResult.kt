package app.mazurov.its.itstestapp.network.model

data class SearchResult(
        val Title: String,
        val Year: String,
        val imdbID: String,
        val Type: String,
        val Poster: String
)