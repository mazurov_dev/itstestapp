package app.mazurov.its.itstestapp.network.model

data class Rating(
        val Source: String,
        val Value: String
)