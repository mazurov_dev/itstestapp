package app.mazurov.its.itstestapp.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.mazurov.its.itstestapp.network.Repository
import app.mazurov.its.itstestapp.network.model.Search

class SearchViewModel : ViewModel() {
    private val repository = Repository
    val movies = MutableLiveData<Search>()
    val isLoading = MutableLiveData<Boolean>()
    val isLoadingFailed = MutableLiveData<Boolean>()


    fun search(q: String) {
        isLoading.postValue(true)
        repository.search(q, {
            movies.postValue(it)
            isLoading.postValue(false)
        }, {
            isLoadingFailed.postValue(true)
            isLoading.postValue(false)
        })
    }
}
