/*
 * @author Mark Mazurov
 * @email mazurov.developer@gmail.com
 * Created 10.08.2018
 */
package app.mazurov.its.itstestapp.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.mazurov.its.itstestapp.R
import app.mazurov.its.itstestapp.network.model.SearchResult
import kotlinx.android.synthetic.main.item_movie.view.*


class SearchAdapter(movies: List<SearchResult>, val onClickListener: (movieId: String) -> Unit) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    var movies: List<SearchResult> = movies
        set(movies) {
            field = movies
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindMovie(movies[position])
    }

    override fun getItemCount(): Int = movies.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindMovie(movie: SearchResult) {
            itemView.tv_movie_title.text = movie.Title

            itemView.setOnClickListener {
                onClickListener(movie.imdbID)
            }
        }
    }
}