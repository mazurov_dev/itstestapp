package app.mazurov.its.itstestapp.ui.fullinformation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.mazurov.its.itstestapp.network.Repository
import app.mazurov.its.itstestapp.network.model.Result



class FullInformationViewModel : ViewModel() {
    private val repository = Repository
    val movie = MutableLiveData<Result>()
    val isLoading = MutableLiveData<Boolean>()
    val isLoadingFailed = MutableLiveData<Boolean>()

    fun getMovie(i: String) {
        isLoading.postValue(true)
        repository.getFullInformation(i, {
            isLoading.postValue(false)
            movie.postValue(it)
        }, {
            isLoadingFailed.postValue(true)
            isLoading.postValue(false)
        })
    }
}
